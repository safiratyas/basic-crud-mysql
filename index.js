const express = require('express');
const path = require('path');
const app = express();
const mysql = require('mysql');

app.use(express.static('public'));
app.use(express.urlencoded({
  extended: false
}));

// Connect The Database
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'electronics',
});

connection.connect(function (error) {
  if (!!error) {
    console.log(error);
  } else {
    console.log('Database Connected!');
  }
});

// Set Views File
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Render Website
app.get('/', (req, res) => {
  res.render('top.ejs');
});

app.get('/index', (req, res) => {
  connection.query(
    'SELECT * FROM list',
    (error, results) => {
      res.render('index.ejs', {
        list: results
      });
    }
  );
});

app.get('/create', (req, res) => {
  res.render('create.ejs');
});

app.post('/insert', (req, res) => {
  connection.query(
    'INSERT INTO list (name,color,brand,price) VALUES (?,?,?,?)',
    [req.body.name, req.body.color, req.body.brand, req.body.price],
    (error, results) => {
      res.redirect('/index');
    }
  );
});

app.get('/edit/:id', (req, res) => {
  connection.query(
    'SELECT * FROM list WHERE id = ?',
    [req.params.id],
    (error, results) => {
      res.render('edit.ejs', {
        list: results[0]
      });
    }
  )
});

app.post('/update/:id', (req, res) => {
  connection.query(
    'UPDATE list SET name=?,color=?,brand=?,price=? WHERE id=?',
    [req.body.name, req.body.color, req.body.brand, req.body.price, req.params.id],
    (error, results) => {
      res.redirect('/index');
    }
  )
});

app.post('/delete/:id', (req, res) => {
  connection.query(
    'DELETE FROM list WHERE id = ?',
    [req.params.id],
    (error, results) => {
      res.redirect('/index');
    }
  )
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`http://localhost:${PORT}`);
});